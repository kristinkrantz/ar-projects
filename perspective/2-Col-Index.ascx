<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>
<%@ Register TagPrefix="ar" TagName="Banner" Src="inc/banner.ascx" %>
<%@ Register TagPrefix="ar" TagName="ContentInfo" Src="inc/contentinfo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Menu" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Logo" Src="~/Admin/Skins/Logo.ascx" %>

<div class="page-template page-template-index page-template-2-col">

  <ar:Banner runat="server" />

  <section class="homepage-hero" data-equalizer>
    <div class="homepage-hero-wrap"></div>
    <dnn:MENU MenuStyle="inc/menu/homepagehero" IncludeNodes="#HomepageHero1" id="HomepageHero" IncludeHidden="true" runat="server"></dnn:MENU>
    <div class="row">
      <div class="homepage-hero-content" data-equalizer-watch>
        <div class="row">
          <div class="logo">
            <dnn:LOGO runat="server" />
          </div>
        </div>
        <div class="homepage-hero-tagline">
          <span class="first">Your</span>
          <span class="second">Business</span>
        </div>
        <div class="support-tagline">
          <span>You&#146;re on the verge of something big.</span>
        </div>
      <div class="homepage-hero-sidebar">
        <div class="row">
          <div class="qq-col">
            <select id="e_1" class="e_1"> 
              <dnn:MENU MenuStyle="inc/menu/quickquote" IncludeNodes="#QuoteSelect1" id="QuickQuoteMenu" runat="server"></dnn:MENU>
            </select>
          </div>
          <div class="qq-col2">
            <input id="e_2" class="e_2 hint" maxlength="10" value="Postal Code" type="text">
          </div>
          <div class="qq-col3">
            <div id="saveForm" class="button success">Learn More.</div>
          </div>
          <script>
            function getQuote(){
            location.href = $("#e_1 option:selected").val() + "#quote/" + $("#e_2").val(); 
            }
            $("#saveForm").click(function () {
            getQuote();
            return false; 
            });
            $("#e_2").keypress(function(a) { 
            if (a.keyCode == 13) { 
            getQuote();
            return false;
            } 
            });
            $("#e_2").focus(function(){
            $(this).removeClass("hint").val("")
            });
          </script>
        </div>
      </div>
    </div>
  </div>
  </section>

  <section role="main">
    <div id="TopPane" class="top" runat="server"></div>
    <div class="row">
      <div id="ContentPane" class="content" runat="server"></div>
      <div id="AsidePane" class="aside" role="complementary" runat="server"></div>
    </div>
    <div id="BottomPane" class="bottom" runat="server"></div>
  </section>

  <ar:ContentInfo runat="server" />
</div>
