<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.SkinObjectBase" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="Logo" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>
<%@ Register TagPrefix="ar" TagName="Init" Src="~/Admin/AgencyRev/Base/Initilization/Initilization2.ascx" %>
<%@ Register TagPrefix="fnn" TagName="Offcanvas" Src="~/Admin/AgencyRev/Framework/Foundation/Components/Offcanvas-2-level.ascx" %>
<%@ Register TagPrefix="fortyfingers" TagName="STYLEHELPER" Src="~/DesktopModules/40Fingers/SkinObjects/StyleHelper/StyleHelper.ascx" %>

<fortyfingers:STYLEHELPER RemoveJsFile="jquery-ui.js,dnn.js,dnn.controls.js,dnncore.js,dnn.modalpopup.js" IfUserMode="None" runat="server" />
<fortyfingers:STYLEHELPER RemoveCssFile="default.css,admin.css" IfUserMode="None" runat="server" />
<fortyfingers:STYLEHELPER AddToHead='<link rel="preconnect" crossorigin href="//use.typekit.net"><link rel="icon" type="image/png" href="/portals/mmorrison/favicon.png" /><script src="https://use.typekit.net/hzr5xwh.js"></script><script>try{Typekit.load({ async: true });}catch(e){}</script>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">
 ' runat="server" />

<ar:Init runat='server'/>
<dnn:DnnCssInclude runat="server" FilePath="dist/css/skin.min.css" PathNameAlias="SkinPath" ForceProvider="DnnPageHeaderProvider" Priority="6" />
<dnn:DnnJsInclude runat="server" FilePath="dist/js/skin.min.js" PathNameAlias="SkinPath" ForceProvider="DnnFormBottomProvider" Priority="4" />

<fnn:Offcanvas MenuNode="*,0,2" Align="left" runat="server" />


<header class="o-banner">
  <div class="u-row" data-equalizer>
    <div class="c-navigation" data-equalizer-watch>
    <div class="c-navigation__phone">
      <a class="c-navigation__phone" href="tel:<avt:MyTokens runat='server' token='[RevTemplate:Standard.TollFreeNumber]' />">
        <i class="fi-telephone"></i> <span><avt:MyTokens runat='server' token='[RevTemplate:Standard.TollFreeNumber]' /></span>
      </a>
    </div>
    <ul class="c-navigation">
      <li><a class="" href="/client-service/claims">Get a Quote</a></li>
      <li><a class="" href="/contact-us">Contact Us</a></li>
  </ul>
    <a role="button" class="left-off-canvas-toggle menu-icon" href="#" ><span>Menu</span></a>
    </div>
  </div>
  <div class="banner-wrap"></div>
</header>

