<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.SkinObjectBase" %>
<%@ Register TagPrefix="dnn" TagName="CurrentDate" Src="~/Admin/Skins/CurrentDate.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Copyright" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Menu" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>

<section class="testimonial">
  <div class="row">
    <div class="testimonial-content">
      <h2>Our Associations:</h2>
      <div class="testimonial-slider">
        <div class="testimonial-item">
          <span class="quote-before"></span><span class="quote-after"></span><br/><span class="testimonial-credit"><a href="https://www.trustedchoice.com/" target="_blank"><img src="/Portals/lajohnson/images/trusted-choice-logo.png"></a></span>
        </div>
        <div class="testimonial-item">
          <span class="quote-before"></span><span class="quote-after"></span><br/><span class="testimonial-credit"><a href="http://www.keystoneinsgrp.com/" target="_blank"><img src="/Portals/lajohnson/images/keystone-insurers-group-fc.png"></a></span>
        </div>
        <div class="testimonial-item">
          <span class="quote-before"></span><span class="quote-after"></span><br/><span class="testimonial-credit"><a href="http://www.bigi.org/default.aspx" target="_blank"><img src="/Portals/lajohnson/images/IIA_2color_CMYK.png"></a></span>
        </div>
      </div>
    </div>
  </div>
</section>
