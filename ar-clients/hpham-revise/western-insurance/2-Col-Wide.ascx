<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>
<%@ Register TagPrefix="ar" TagName="Banner" Src="inc/banner.ascx" %>
<%@ Register TagPrefix="ar" TagName="ContentInfo" Src="inc/contentinfo.ascx" %>
<%@ Register TagPrefix="ar" TagName="DropTop" Src="inc/drop-top.ascx" %> <%@ Register TagPrefix="ar" TagName="DropBottom" Src="inc/drop-bottom.ascx" %> <ar:DropTop runat="server" />

<div class="page-template page-template-wide page-template-2-col-wide">
<ar:Banner runat="server" />
<section class="header-wrap" style="background-image: url(<avt:MyTokens runat='server' Token='[Tab:Iconfilelarge]'/>)">
	<div class="wide-wrap"></div>
  	<div class="row">
    <a class="logo" href="/home"><img src="<%= PortalSettings.HomeDirectory %><%= PortalSettings.LogoFile %>" alt="<%= PortalSettings.PortalName %>"</a>
  </div>
  <div class="row medium-12 columns headline">
	<%= PortalSettings.ActiveTab.TabName %>
  </div>
</section>
<section role="main">
  <div class="row">
    <div id="AsidePane" class="aside" runat="server"></div>
    <div id="ContentPane" class="content" runat="server"></div>
  </div>
</section>

<ar:ContentInfo runat="server" />
</div>

<ar:DropBottom runat="server" />
