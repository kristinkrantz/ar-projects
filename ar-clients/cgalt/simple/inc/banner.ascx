<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.SkinObjectBase" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="Logo" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>
<%@ Register TagPrefix="ar" TagName="Init" Src="~/Admin/AgencyRev/Base/Initilization/Initilization2.ascx" %>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<%@ Register TagPrefix="fortyfingers" TagName="STYLEHELPER" Src="~/DesktopModules/40Fingers/SkinObjects/StyleHelper/StyleHelper.ascx" %>
<fortyfingers:STYLEHELPER RemoveCssFile="portal.css"  runat="server" />
<fortyfingers:STYLEHELPER RemoveCssFile="default.css,admin.css" IfUserMode="None" runat="server" />
<fortyfingers:STYLEHELPER RemoveJsFile="jquery-ui.js,dnn.js,dnn.controls.js,dnncore.js,dnn.modalpopup.js" IfUserMode="None" runat="server" />
<ar:Init runat='server'/>
<dnn:DnnCssInclude runat="server" FilePath="dist/css/skin.min.css" PathNameAlias="SkinPath" ForceProvider="DnnPageHeaderProvider" Priority="6" />
<dnn:DnnJsInclude runat="server" FilePath="dist/js/skin.min.js" PathNameAlias="SkinPath" ForceProvider="DnnFormBottomProvider" Priority="4" />
<dnn:DnnCssInclude runat="server" FilePath="inc/plugins/font-awesome/css/font-awesome.min.css" PathNameAlias="SkinPath" ForceProvider="DnnPageHeaderProvider" Priority="6" />
<header role="banner">
  <div class="header-wrap row" data-equalizer>
    <a role="button" class="right-off-canvas-toggle offcanvas-menu-icon" href="#"><span></span> Menu</a>
    <a class="logo" href="/"><img src="http://www.galtinsurance.com/Portals/cgalt/Images/logo-reverse.png"></a>
    <div class="c-banner__navigation">
      <div class="c-banner__mini-nav hide-for-small-only">
        <ul class="c-banner__mini-nav">
          <li><a href="/contact-us/meet-our-team">Join Our Team</a></li>
        </ul>
      </div>
      <div class="c-banner__phone-md hide-for-small-only">
        <avt:MyTokens runat='server' token='[RevTemplate:Standard.TollFreeNumber]' />
      </div>

    </div>
    <div class="c-banner__phone-sm hide-for-medium-up">
      <a href="tel:<avt:MyTokens runat='server' token='[RevTemplate:Standard.TollFreeNumber]' />"><i class="fa fa-phone"></i></a>
    </div>
  </div>
</header>

