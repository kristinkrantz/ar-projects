$(document).foundation({ offcanvas: { open_method: 'overlap' } });

$(function() {
  'use strict';
  $(':input','.addressAdv').attr('placeholder', 'Apt.');
  // convert quote form labels to placeholders
  $(':input','.field').each(function(index, elem) {
    var eId = $(elem).attr('id');
    var label = null;
    if (eId && (label = $(elem).parents('form').find('label[for='+eId+']')).length === 1) {
        $(elem).attr('placeholder', $(label).html());
        $(label).remove();
    }
  });

  var offCanvasMighty = require('./offcanvas-mighty.js');

  // remove inline styles from state select
  $('select', '.state').removeAttr( 'style' );

});
(function () {
  'use strict';
   var previousScroll = 0;
   $(window).scroll(function () {
      var currentScroll = $(this).scrollTop();
      if (currentScroll < previousScroll || currentScroll < 100){
           $('.menu-icon').removeClass('hide');
      }
      else {
           $('.menu-icon').addClass('hide');
      }
      previousScroll = currentScroll;
   });

   $('.menu-icon').click(function () {
       $('body').animate({ scrollTop: 0 }, 350);
       $('.header footer menu').addClass('expand');
   });

}());
