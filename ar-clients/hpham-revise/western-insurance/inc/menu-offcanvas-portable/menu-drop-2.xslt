<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:param name="Align"><![CDATA[left]]></xsl:param>
  <xsl:template match="/*">
    <xsl:apply-templates select="root" />
  </xsl:template>
  <xsl:template match="root">
    <aside class="{$Align}-off-canvas-menu">
      <div class="off-canvas-header"><a class="exit-off-canvas"></a></div>
      <ul class="off-canvas-list">
        <xsl:apply-templates select="node">
          <xsl:with-param name="level" select="0"/>
        </xsl:apply-templates>
      </ul>
    </aside>
    <a class="exit-off-canvas"></a>
  </xsl:template>
  <xsl:template match="node">
    <xsl:param name="level" />
    <xsl:choose>
      <xsl:when test="$level=0">

        <li class="off-canvas-submenu-call">

          <xsl:attribute name="class">
            <xsl:if test="@breadcrumb = 1">active</xsl:if>
            <xsl:if test="node">
              <xsl:text>has-dropdown</xsl:text>
            </xsl:if>
          </xsl:attribute>

          <xsl:choose>
            <xsl:when test="@enabled = 1">
              <a href="{@url}">
                <xsl:attribute name="class">
                  <xsl:if test="node">
                    <xsl:text>off-canvas-submenu-call</xsl:text>
                  </xsl:if>
                </xsl:attribute>
                <xsl:value-of select="@text" />
              </a><div class="toggle-me"></div>
            </xsl:when>
            <xsl:otherwise>
              <a href="#">
                <xsl:value-of select="@text" />
              </a>
            </xsl:otherwise>
          </xsl:choose>

          <xsl:if test="node">
            <ul class="off-canvas-submenu">
              <xsl:apply-templates select="node">
                <xsl:with-param name="level" select="$level + 1" />
              </xsl:apply-templates>
            </ul>
          </xsl:if>

        </li>
      </xsl:when>
      <xsl:otherwise>
        <li>

          <xsl:choose>
            <xsl:when test="@enabled = 1">
              <a href="{@url}">
                <xsl:value-of select="@text" />
              </a>
              <xsl:if test="node">
                <ul class="off-canvas-submenu">
                  <xsl:apply-templates select="node" />
                </ul>
              </xsl:if>
            </xsl:when>
            <xsl:otherwise>
              <a href="#">
                <xsl:value-of select="@text" />
              </a>
              <xsl:if test="node">
                <ul class="off-canvas-submenu">
                  <xsl:apply-templates select="node" />
                </ul>
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose>

        </li>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
