<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.SkinObjectBase" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>

<div class="catch">
	<div class="row">
	  <div class="catch-message">
	    <h5>WE CARE ABOUT YOU</h5>
	    <p>Life happens, and we are there for you when it does. 7 days a week we strive to protect you every step of the way.</p>
	  </div>
	  <div class="catch-actions">
	    <div class="catch-button-group">
	      <a class="button success radius" href="/insurance-quotes">Get a Quote Today</a>
	    </div>
	  </div>
	 </div>
</div>
