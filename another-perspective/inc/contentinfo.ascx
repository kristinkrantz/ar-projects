<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.SkinObjectBase" %>
<%@ Register TagPrefix="dnn" TagName="CurrentDate" Src="~/Admin/Skins/CurrentDate.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Copyright" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Login" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Menu" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>

<footer class="o-footer u-hidden@print">
<div class="o-footer__top">
  <div class="u-row">
    <div class="o-footer__catch">
      <h4>It's time to protect what matters most.</h4>
      <a role="button" href="/personal-insurance">Get a Quote</a>
    </div>
  </div>
</div>
<div class="o-footer__middle">
  <div class="u-row">
    <div class="o-footer__companyinfo">
      <div class="o-footer__logo">
        <img src="<%= PortalSettings.ActiveTab.SkinPath %>images/logo--inverse.png" alt="<%= PortalSettings.PortalName %>">
      </div>
      <div class="o-footer__address">
        <ul class="o-footer__address__list">
          <li>
            <div itemscope itemtype="http://schema.org/LocalBusiness">
              <span class="hide" itemprop="name"><%= PortalSettings.PortalName %></span>
              <span class="hide" itemprop="description"><%= PortalSettings.Description %></span>
              <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span itemprop="streetAddress"><avt:MyTokens runat='server' token='[RevTemplate:Standard.StreetAddress]' /></span>, 
                <span itemprop="addressLocality"><avt:MyTokens runat='server' token='[RevTemplate:Standard.City]' /></span>,
                <span itemprop="addressRegion"><avt:MyTokens runat='server' token='[RevTemplate:Standard.StateAbbr]' /></span>
                <span itemprop="telephone" class="right"><avt:MyTokens runat='server' token='[RevTemplate:Standard.LocalNumber]' /></span>
              </div>
            </div>
          </li>
          <li>G3-181 Perry Street, Port Perry, ON <span class="right">905.985.8078</span></li>
          <li>76 King Street, Woodville, ON <span class="right">705.439.2406</span></li>
          <li>199 Sherbrooke Street, Peterborough, ON <span class="right">705.743.3382</span></li>
          <li>47 Colborne Street, Fenelon Falls, ON <span class="right">705.887.2517</span></li>
          <li>79 Bolton Street, Bobcaygeon, ON <span class="right">705.738.6000</span></li>
        </ul>
      </div>
    </div>
    <div class="o-footer__widget o-footer__widget--news js-footer__widget--news">
      <h6>Recent News</h6>
      <ul class="o-footer__widget__list"></ul>
    </div>
    <div class="o-footer__widget o-footer__widget--nav">
      <h6>Need More?</h6>
      <ul class="o-footer__widget__list">
        <dnn:Menu MenuStyle="/admin/AgencyRev/Framework/Foundation/Menus/menu-list" NodeSelector="*,0,2" ExcludeNodes="Home,Admin,Revolution" runat="server" ></dnn:Menu>
        <li><dnn:Login runat="server" id="dnnLogin" Text="Sign In" LogoffText="Sign Out" /></li>
      </ul>
    </div>
  </div>
</div>
<div class="o-footer__bottom">
  <div class="u-row">
    <div class="o-footer__credit">
      <p class="copyright">&copy; <dnn:Copyright id="Copyright" runat="server" /> Made by <a href="http://www.agencyrevolution.com">Agency Revolution</a> in Oregon</p>
    </div>
    <div class="o-footer__social">
      <ul class="social">
        <li><a class="fi-social-facebook" href="https://www.facebook.com/pages/Stewart-Morrison-Insurance/195630117185107/" target="_blank"></a></li>
        <li><a class="fi-social-twitter" href="https://www.twitter.com/smibrokers/" target="_blank"></a></li>
        <li><a class="fi-social-linkedin" href="https://www.linkedin.com/company/stewart-morrison-insurance-brokers-ltd-/" target="_blank"></a></li>
        <li><a class="fi-social-google-plus" href="https://plus.google.com/116859789766163366728/" target="_blank"></a></li>
      </ul>
    </div>
  </div>
</div>
</footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/headroom/0.7.0/headroom.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/headroom/0.7.0/jQuery.headroom.min.js"></script>
