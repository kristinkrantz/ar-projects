<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>
<%@ Register TagPrefix="ar" TagName="Banner" Src="inc/banner.ascx" %>
<%@ Register TagPrefix="ar" TagName="ContentInfo" Src="inc/contentinfo.ascx" %>

<ar:Banner runat="server" />

<div class="s-cms-content s-cms-content--layout">
  <div id="TopPane" class="o-top-pane" runat="server"></div>
  <div class="u-row">
    <div id="AsidePane" class="o-aside-pane" role="complementary" runat="server"></div>
    <div id="ContentPane" class="o-content-pane" runat="server"></div>
  </div>
  <div id="BottomPane" class="o-bottom-pane" runat="server"></div>
</div>

<ar:ContentInfo runat="server" />

