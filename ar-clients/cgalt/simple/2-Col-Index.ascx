<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>
<%@ Register TagPrefix="ar" TagName="Banner" Src="inc/banner.ascx" %>
<%@ Register TagPrefix="ar" TagName="ContentInfo" Src="inc/contentinfo.ascx" %>
<%@ Register TagPrefix="fnn" TagName="RelatedMenu" src="~/admin/AgencyRev/Framework/Foundation/Extensions/Related-content.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Menu" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="ar" TagName="DropTop" Src="inc/drop-top.ascx" %>
<%@ Register TagPrefix="ar" TagName="DropBottom" Src="inc/drop-bottom.ascx" %>

<ar:DropTop runat="server" />

<div class="page-template page-template-index page-template-2-col">
  <ar:Banner runat="server" />

  <section class="homepage-hero" style="background-image: url(<avt:MyTokens runat='server' Token='[Tab:Iconfilelarge]' />)" data-equalizer>
    <div class="row full-width" data-equalizer-watch>
      <dnn:MENU MenuStyle="inc/menu/homepagehero" IncludeNodes="#HomepageHeroIndex" id="HomepageHero" runat="server"></dnn:MENU>
      <div class="homepage-hero-content" >
        <div class="homepage-hero-header" >
          <h2><span class="c__h21">Achieve</span>
            <br/>
            <span class="c__h22"> Greatness.</span></h2>
          <span class="c__tagline">Explore the best approach to insurance.</span>
        </div> 
      </div>
    </div>
    <div class="feature-wrap-color"></div>
    <div class="small-12 full-width columns text-center">
      <div id="NichePane" class="featureNiches" runat="server"></div>
      <div id="FeaturePane" class="feature" runat="server"></div>
    </div>
    <div class="quick-quote">
      <div class="qq-col">
        <select id="e_1" class="e_1"> 
          <fnn:RelatedMenu MenuStyle="inc/menu/quick-quote" IncludeNodes="#QuoteSelect" id="QuickQuoteMenu" runat='server'/>
        </select>
      </div>
      <div class="qq-col2">
        <input id="e_2" class="e_2 hint" maxlength="7" value="Zip Code" type="text">
      </div>
      <div class="qq-col3">
        <div id="saveForm" class="button alert expand">Get a Quote<i class="fa fa-chevron-right"></i></div>
      </div>
      <script>
        function getQuote(){ 
        location.href = $("#e_1 option:selected").val() + "#quote/" + $("#e_2").val(); 
        }
        $("#saveForm").click(function () {
        getQuote();
        return false; 
        });
        $("#e_2").keypress(function(a) { 
        if (a.keyCode == 13) { 
        getQuote();
        return false;
        }
        });
        $("#e_2").focus(function(){
        $(this).removeClass("hint").val("")
        });
      </script>
    </div>
  </section>

  <section role="main">

    <div id="TopPane" class="top" runat="server"></div>
      <div class="c-top small-12 columns text-center">
        <h4>Let's Talk.</h4>
        <div class="social-top">
          <a href="https://www.facebook.com/GaltInsuranceGroup" target="_blank"><i class="fa fa-facebook"></i></a><a href="https://instagram.com/galtinsurancegroup/" target="_blank"><i class="fa fa-instagram"></i></a> <a href="https://twitter.com/galtinsurance" target="_blank"><i class="fa fa-twitter"></i></a><a href="https://www.linkedin.com/company/galt-insurance-group" target="_blank"><i class="fa fa-linkedin"></i></a><a href="https://www.youtube.com/user/GaltInsuranceGroup" target="_blank"><i class="fa fa-youtube"></i></a><a href="https://plus.google.com/117127155493320694087/posts" target="_blank"><i class="fa fa-google-plus"></i></a>
      </div>
        <a class="button alert" href="/personal">Get a Quote</a>
    </div>
    <div class="row">
      <div id="ContentPane" class="content" runat="server"></div>
      <div id="AsidePane" class="aside" role="Complementary" runat="server"></div>
    </div>
    <div id="BottomPane" class="bottom" runat="server"></div>
  </section>

  <ar:ContentInfo runat="server" />
</div>

<ar:DropBottom runat="server" />

