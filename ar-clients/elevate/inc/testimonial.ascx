<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.SkinObjectBase" %>
<%@ Register TagPrefix="dnn" TagName="CurrentDate" Src="~/Admin/Skins/CurrentDate.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Copyright" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Menu" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>

<section class="testimonial">
  <div class="row">
    <div class="testimonial-content">
      <h2>What our clients say about us:</h2>
      <div class="testimonial-slider">
        <div class="testimonial-item">
          <span class="quote-before">“</span>First Testimonial Here...<span class="quote-after">”</span><br/><span class="testimonial-credit">&#8212; Testimonial Provider</a>
        </div>
        <div class="testimonial-item">
          <span class="quote-before">“</span>Second Testimonial Here...<span class="quote-after">”</span><br/><span class="testimonial-credit">&#8212; Testimonial Provider</a>
        </div>
      </div>
    </div>
  </div>
</section>
