<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>
<%@ Register TagPrefix="ar" TagName="Banner" Src="inc/banner.ascx" %>
<%@ Register TagPrefix="ar" TagName="ContentInfo" Src="inc/contentinfo.ascx" %>

<ar:Banner runat="server" />

<div class="s-cms-content s-cms-content--feature">
  <div class="o-feature-wrap" style="background-image: url(<avt:MyTokens runat='server' Token='[Tab:Iconfilelarge]' />?dw=1200&tw=700&mw=480);">
    <div class="u-row-wrap">
      <div class="u-row">
        <div id="FeaturePane" class="o-feature-pane" runat="server"></div>
      </div>
    </div>
  </div>

  <section class="main" role="main">
    <div id="TopPane" class="o-top-pane" runat="server"></div>
    <div class="u-row">
      <div id="AsidePane" class="o-aside-pane" role="complementary" runat="server"></div>
      <div id="ContentPane" class="o-content-pane" runat="server"></div>
    </div>
    <div id="BottomPane" class="o-bottom-pane" runat="server"></div>
  </section>
</div>

<ar:ContentInfo runat="server" />
