<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-5">
  [*>NODE-TOP]
</ul>

[>NODE-TOP]
  <li class="item">
  [?ENABLED]
    <a href="[=URL]">
      <div class="item-frame">
        <img class="item-image" src="[=ICON]" alt="[=TEXT]">
      </div>
    </a>
    <a href="[=URL]">
      <h3 class="item-name">[=TEXT]</h3>
    </a>
    <p class="item-description">[=DESCRIPTION]</p>
    
  [?ELSE]
    [=TEXT]
  [/?]
  </li>
[/>]
