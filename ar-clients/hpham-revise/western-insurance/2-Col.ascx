<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>
<%@ Register TagPrefix="ar" TagName="Banner" Src="inc/banner.ascx" %>
<%@ Register TagPrefix="ar" TagName="ContentInfo" Src="inc/contentinfo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="ar" TagName="DropTop" Src="inc/drop-top.ascx" %> <%@ Register TagPrefix="ar" TagName="DropBottom" Src="inc/drop-bottom.ascx" %> 
<ar:DropTop runat="server" />
<script runat="server">

  public string TabName {get;set;}

  protected void Page_Load(object sender, System.EventArgs e) {
    TabName = PortalSettings.ActiveTab.TabName;
  }

</script>

<div class="page-template page-template-2-col-quote">
<ar:Banner runat="server" />

<section class="feature-wrap" style="background-image: url(<avt:MyTokens runat='server' Token='[Tab:Iconfilelarge]' />)">
  <div class="feature-wrap-color"></div>
  <div class="row">
    <a class="logo" href="/home"><img src="<%= PortalSettings.HomeDirectory %><%= PortalSettings.LogoFile %>" alt="<%= PortalSettings.PortalName %>"</a>
  </div>
  <div class="row">
    <div class="small-10 medium-7 large-6 columns text-center">
     <!--  <h2 class="subheader"><%= PortalSettings.ActiveTab.Description %></h2> -->
      <div id="FeaturePane" class="feature" runat="server"></div>
    </div>
  </div>
  <div class="row embelishment">
    <h1 class="inner-head small-12 columns">
    <%= PortalSettings.ActiveTab.TabName %>
    </h1>
  </div>
</section>

<section role="main">
  <div id="TopPane" class="top" runat="server"></div>
  <div class="row">
    <div id="Asidepane" class="aside" runat="server"></div>
    <div id="ContentPane" class="content" runat="server"></div>
    </div>
  <div id="BottomPane" class="bottom" runat="server"></div>
</section>

<ar:ContentInfo runat="server" />
</div>

<ar:DropBottom runat="server" />
