
$('.niche-slider').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2
});

$('.testimonial-slider').slick({
  autoplay: true,
  autoplaySpeed: 5000,
  dots: false,
  fade: true,
  infinite: true,
  speed: 300,
  arrows: 'false',
  slide: 'div',
  cssEase: 'linear'
});
