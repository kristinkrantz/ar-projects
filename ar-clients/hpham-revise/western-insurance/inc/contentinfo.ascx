<%@ Control language="c#" AutoEventWireup="true" Explicit="True" Inherits="DotNetNuke.UI.Skins.SkinObjectBase" %>
<%@ Register TagPrefix="dnn" TagName="CurrentDate" Src="~/Admin/Skins/CurrentDate.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Copyright" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Login" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Menu" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="avt" TagName="MyTokens" Src="~/DesktopModules/avt.MyTokens/SkinObjectReplacer.ascx" %>

<div class="panel footer-top">
<div class="row">
  <div class="small-12 medium-7 columns">
    <h2><em>For a better tomorrow.</em></h2>
  </div>
  <div class="medium-5 columns text-right">
    <a class="button large" href="/for-insurance-quotes">Quote Today<span class="arrow">&#x2192;</span></a>
  </div>
</div>
</div>
<div class="footer-bottom text-center">
  <ul class="medium-8 large-5 medium-centered large-centered columns inline-list">
    <dnn:Menu MenuStyle="/admin/AgencyRev/Framework/Foundation/Menus/menu-list" NodeSelector="*,0,2" ExcludeNodes="Home,Research Center,Client Service,Admin,Revolution" runat="server" ></dnn:Menu>
    <li><dnn:Login runat="server" id="dnnLogin" Text="Sign In" LogoffText="Sign Out" /></li>
  </ul>

  <p class="copyright">Made with <span class="heart">&#x2665;</span> by <a href="http://www.agencyrevolution.com">Agency Revolution</a> in Oregon. &copy; <dnn:Copyright id="Copyright" runat="server" /></p>

</div>
