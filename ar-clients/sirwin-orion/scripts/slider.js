$('.slider-main').slick({
  autoplay: true,
  autoplaySpeed: 5000,
  dots: false,
  fade: true,
  infinite: true,
  speed: 300,
  slide: 'div',
  arrows: 'false',
  cssEase: 'linear'
});
