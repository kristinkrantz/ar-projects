$('.slider-main').slick({
  autoplay: true,
  autoplaySpeed: 5000,
  dots: false,
  slidesToShow: 1,
  infinite: true,
  speed: 300,
  slide: 'div',
  cssEase: 'linear'
});

